package com.bugaj.przemek;

public class Novel {

    private String title;
    private int amountMarks;
    private int sumMarks;
    private double averageMarks;

    public Novel(String title){
        amountMarks = 0;
        sumMarks = 0;
        averageMarks = 0;
        this.title = title;
    }
    public String getTitle(){
        return title;
    }

    public int getSumMarks(){
        return sumMarks;
    }
    public int getAmountMarks(){
        return amountMarks;
    }
    public double getAverageMarks(){
        if (0==amountMarks) {
            System.out.println("Brak ocen");
        }
        else {
            averageMarks = sumMarks / amountMarks;
        }
        return averageMarks;
    }

    public void addMark(int mark){
        sumMarks = sumMarks+ mark;
    }
}

package com.bugaj.przemek;

public class MarkSystem {

    public User[] allUsers;
    public Novel[] allNovels;
    public MarkSystem(int userAmount, int novelAmount){
        allUsers = new User [userAmount];
        allNovels = new Novel [novelAmount];
    }
    public void serializeUser (User user){
        for (int i = 0; i < allUsers.length; i++){
            if (allUsers[i]==null) {
                allUsers[i]=user;
                break;
            }
        }
    }
    public void serializeNovel (Novel novel){
        for (int i = 0; i < allNovels.length; i++){
            if (allNovels[i]==null) {
                allNovels[i]=novel;
                break;
            }
        }
    }
}

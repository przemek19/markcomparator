package com.bugaj.przemek;

public class User {

    String name;
    int age;
    boolean isfemale;

    public User (String name, int age, boolean isfemale){
        this.name = name;
        this.age = age;
        this.isfemale = isfemale;
    }

    public Novel markNovel(Novel novel, int mark){
        novel.addMark(mark);
        return novel;
    }

}
